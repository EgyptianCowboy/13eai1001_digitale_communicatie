\section{NuBus}
\label{sec:nubus}
\subsection{Background}
\label{ssec:nubusBack}
NuBus is a 32-bit parallel computer bus.
It was first introduced at MIT in cooperation with Western Digital in 1979-1983, NuBus was a big step forwards when first introduced in comparison with the 8-bit busses of the time.

NuBus also introduced four identification signals.
This allowed the host to identify the card at start-up. \cite{nubusMac}
This means that the user doesn't have to configure the system.
For instance, with ISA, which will be discussed in section \ref{sec:pc104}, the driver has to be configured not only for the card, but for any memory it required, the interrupts it used and so on.
NuBus doesn't require any of that, making it one of the first plug-and-play architectures.

The downside of this is, while it made using NuBus easier for users and device driver authors, tt makes life more difficult for designers of the cards.
It required adding a NuBus controller chip between the bus and any I/O chips on the card, thus increasing the cost. \\

NuBus is a synchronous (10 MHz), multiplexed, multi master bus that provides an arbitration mechanism.
A later addition, NuBus 90, increased the clock speed to 20 MHz.
The only bus transfers are the read and write transfers (and block transfer versions of each of these) to a single 32-bit address space.
Within these mechanisms NuBus accomplishes I/O and interrupts.
The modules attached to the NuBus are peers.
No card or slot position is the default master or ``special slot''.
The exception is that only one card drives the system clock.
Each slot has an ID hard wired into the backplane.
Geographic slot addressing and a non-daisy-chain arbitration scheme make system configuration simpler by eliminating switches and jumpers.
This minimalist approach results in a conceptually straightforward bus with a small pin count (51 active signal lines). \cite{ieeeNubus}

Although it is a synchronous bus, it has many features of an asynchronous bus.
Transactions may be a variable number of clock periods long.
This provides the adaptability of an asynchronous bus with the design simplicity of a synchronous bus. \\

Figure \ref{fig:nubusdiag} shows the major elements of a typical NuBus system.
The NuBus supports multiprocessing and other system architectures with a simple mechanisms.
The diagram shown in figure \ref{fig:nubusdiag} is complete with the addition of only eight more lines. The ID, clock, address/data and ARB lines are supplementen with system reset, parity and data transfer control lines.

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{./images/nubusdiag.png}
\caption{Simplified NuBus diagram \cite{nubusSpec}}
\label{fig:nubusdiag}
\end{figure}

Nowadays NuBus isn't used any more.
PCI and other parallel busses obsoleted the NuBus.
The IEEE 1196-1987 standard standardised the NuBus, published in 08-08-1988. But it was withdrawn in 03-06-2000.

\subsection{Specifications}
\label{ssec:nubusSpec}
NuBus is a synchronous bus because the signal timing phases with a fixed system clock. This system clock has a duty cycle of 25\% and a frequency of 10MHz.

Table \ref{tab:nupin} visualises pin assignments.

\begin{table}[H]
  \begin{center}
    \caption{NuBus Pin Assignment}
    \label{tab:nupin}
    \begin{tabular}{r|ccc}
      \rowcolor{gray!50}
        &A    &B   &C     \\\hline
      1 &-12  &-12 &RESET*\\
      2 &RSVD &GND &RSVD  \\
      3 &SPV* &GND &+5    \\
      4 &SP*  &+5  &+5    \\
      5 &TMI* &+5  &TM0*  \\
      6 &AD1* &+5  &AD0*  \\
      7 &AD3* &+5  &AD2*  \\
      8 &AD5* &-5.2&AD4*  \\
      9 &AD7* &-5.2&AD6*  \\
      10&AD9* &-5.2&AD8*  \\
      11&AD11*&-5.2&AD10* \\
      12&AD13*&GND &AD12* \\
      13&AD15*&GND &AD14* \\
      14&AD17*&GND &AD16* \\
      15&AD19*&GND &AD18* \\
      16&AD21*&GND &AD20* \\
      17&AD23*&GND &AD22* \\
      18&AD25*&GND &AD24* \\
      19&AD27*&GND &AD26* \\
      20&AD29*&GND &AD28* \\
      21&AD31*&GND &AD30* \\
      22&GND  &GND &GND   \\
      23&GND  &GND &PFW*  \\
      24&ARB1*&-5.2&ARB0* \\
      25&ARB3*&-5.2&ARB2* \\
      26&ID1* &-5.2&ID0*  \\
      27&ID3* &-5.2&ID2*  \\
      28&ACK* &+5  &START*\\
      29&+5   &+5  &+5    \\
      30&RQST*&GND &+5    \\
      31&NMRQ*&GND &GND   \\
      32&+12  &+12 &CLK*  \\
    \end{tabular}
  \end{center}
\end{table}

These signals are grouped into five classes based on the functions they perform.
There are also power and ground lines.
Table \ref{tab:nusig} shows these categories.

\begin{table}[H]
  \begin{center}
    \caption{NuBus signals}
    \label{tab:nusig}
    \begin{tabular}{l|r}
      \rowcolor{gray!50}
      Category        & Number of pins \\\hline
      Utility signals & 4\\
      Transaction control signals & 4\\
      Slot identification & 4\\
      Address/data/parity & 34\\
      Arbitration signals & 5\\
      Power & 23\\
      Ground/reserved & 22\\
    \end{tabular}
  \end{center}
\end{table}

\subsection{NuBus transaction}
\label{ssec:nubustrans}
The ID<3\ldots0>* are binary coded to support addressing of up to 16 modules.
This means that NuBus does not require switches or jumpers to convey addressing information.\cite{nubusMac}
The address and data lines AD<31\ldots0>* are multiplexed to convey address information during the start cycle of a transaction.
The data size, transfer direction and byte lanes are indicated during the start cycle by the transfer mode lines TM<1,0>* in conjunctions with AD<1,0>*.
The ack cycle terminates a transaction. During this ack cycle the transfer mode lines are coded to signal normal completion or fail conditions.
If the failure is temporary, the master is encouraged to try again later.\cite{nubusMac}

This concludes a NuBus transaction.
Such a transaction is visualised in figure \ref{fig:nubustrans}.

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{./images/NubusTransaction.png}
\caption{NuBus signal timing \cite{nubusMac}}
\label{fig:nubustrans}
\end{figure}

NuBus support multiple masters with distributed arbitration on the bussed lines RQST* and ARB<3\ldots0>* in conjunction with ID<3\ldots0>*.
This eliminates any jumpering required in older systems.
In this arbitration scheme all masters are peers.
But any master may bus lock at any time.
Within a bus lock a master may also lock any shared resources.
Resource locking and unlocking is controlled by issuing an attention cycle.
The attention cycle is issued by asserting the START* and ACK* together.

By phasing most bus signal transitions with the system clock NuBus avoids metastability issues.
But attention has to paid to external asynchronous inputs.

\subsection{Conclusion}
\label{ssec:nubusconc}
With its jumper and switch-free conception, arbitration and single indirect connector NuBus was a serious improvement compared to contemporary competitors.
