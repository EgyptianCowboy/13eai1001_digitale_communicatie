\section{FireWire}
\label{sec:firewire}
Around 1986 ``Apple Computer Inc.'' Developed a fast, but low cost serial bus architecture called ``FireWire''. \cite{fwjaycar}
It was designed to address several issues:
\begin{itemize}
\item Parallel buses are expensive. Apple chose to create a serial bus with massive bandwidth capabilities.
\item Apple expected an increased market in consumer electronics, and Apple wanted a standardized bus for them.
\item They wanted devices to be self-configurable (Plug and Play) on connecting the bus.
\item The entire bus should be independent of the host system. This eliminates the host processor and memory bottleneck.
\item They also wanted to reserve bus bandwidth (isochronous).
\end{itemize}
FireWire devices are generally multimedia based.
However, they can also be general PC peripherals.
\\
FireWire has the following data transfer speeds \cite{fwSpec}:
\begin{itemize}
\item 100 Mbps labelled 'S100',
\item 200 Mbps labelled 'S200',
\item 400 Mbps labelled 'S400',
\item 800 Mbps labelled 'S800'.
\end{itemize}

Apple's work caught the eyes of other vendors and an IEEE committee was formed to create a formal standard.
The IEEE standard is the 1394 standard.

\subsection{Terminology}
\label{ssec:fwterm}
The IEEE 1394 standard creates a small amount of terminology that must be discussed first. \cite{fwlinux}
A module is one device attached to the bus, containing at least one node.
A node is a logical entity within a module.
A unit is a functional subcomponent of a node that can identify processing, memory or I/O functionality.
Units of a node generally operate independently, this means they have their own drivers.
But they may share registers with each other.
Figure \ref{fig:fwmodule} shows the architecture of such a module.
A port is a point of I/O within a node, where the node plugs into the bus.
Nodes can be single or multi-ported.
Packets are transferred in a peer-to-peer fashion.
This means that when a multi-ported node receives a packet, it is retransmitted over its others ports.
Each time a device is plugged into or unplugged from the bus, the entire bus topology is reconfigured.
Each bus can be viewed as a tree of devices with one and only one root.
The device at the root of the tree is the root node.

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{./images/fwmodule.jpg}
\caption{Module architecture \cite{fwlinux}}
\label{fig:fwmodule}
\end{figure}

\subsection{Address Space}
\label{ssec:fwadd}
FireWire uses a 64-bit addressing model.
The address space is divided into equal space for 64k nodes. \cite{fwlinux}
These nodes are divided into 1024 buses with 64 nodes per bus. \cite{fwlinux}
Bits <0\ldots9> define the target bus, bits <10\ldots15> identify the target node on the bus.
And bits <16\ldots63> define 256 terabytes of memory address space for each node.
\\
There can only be 63 nodes on each bus, because the last node is used as a broadcast address.
\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{./images/fwaddress.png}\caption{1394 64-bit Address \cite{fwlinux}}
\label{fig:fwaddress}
\end{figure}

\subsection{Transfer}
\label{ssec:fwtrans}
IEEE 1394 defines two methods of transfer:
\begin{itemize}
\item \textit{Asynchronous Transfer Mode} allows for periodic data transfer with guaranteed delivery. \cite{fwlinux}
\item \textit{Isochronous Transfer Mode} is best-effort delivery used to deliver data across the bust at a constant rate. \cite{fwlinux}
\end{itemize}

The transfer method used depends on the nature of the data being transferred.
For example: if data has to be streamed across the FireWire bus, the isochronous transfer mode would be used because the bandwidth is known a priori and data needs to be sent at constant intervals.
And if data has to be copied the asynchronous transfer method would be used because no data has to be lost.

\subsection{Line coding}
\label{ssec:fwlinec}
All data is sent in serial four byte words.
These are called quadlets.
The quadlets are encoded together with their clock signal onto two non return to zero (NRZ) bus signals, using a technique called Data-Strobe (DS) coding.
This improves transmission reliability by only changing one of the two signals in each data bit period. \cite{fwjaycar}\\

Figure \ref{fig:fwds} shows how DS coding works.
The top waveform shows the data bits, the bottom waveform shows the clock signal.
At the transmitting node these two waveforms are fed to an XOR-gate to generate the strobe signal.
The strobe and data signal are sent along the bus.
Being fed through an XOR-gate means that there's only one change in the pair every bit period.\\

At the receiving device the data and strobe signals are again fed through an XOR-gate.
The output of this gate reconstructs the clock signal with its timing still locked to the data.
This means that DS-coding avoids the need for any PLL at the receiver.

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{./images/fwDCCode.png}\caption{DS Coding \cite{fwjaycar}}
\label{fig:fwds}
\end{figure}

\subsection{Twisted pairs}
\label{ssec:fwtp}
The IEEE 1394 data and strobe signals are sent on cables with two separately shielded twisted-wire pairs.
One is called TPA (twisted pair A) and the other TPB (twisted pair B).
Transfers in one direction have the data signal on TPA and the strobe signal on TPB.
Transfers in the other direction have the data signal on TPB and the strobe signal on TPA.
Figure \ref{fig:fwstrobedata} visualises this principle.\cite{fwjaycar}\\

There are two kinds of IEEE 1394 cable, with matching connectors.
The primary cable not only has TPA and TPB, but also a two DC power wires.
A cross-section of this cable is visible in figure \ref{fig:fwtp}.
The IEEE 1394 standard allows nodes to be supplied with DC power via the two wires.
The power can be supplied at a voltage between 8 and 33V, with a current of up to 1.5A. \cite{fwjaycar}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{./images/fwStrobeData.png}\caption{Strobe and Data signals \cite{fwjaycar}}
\label{fig:fwstrobedata}
\end{figure}

The second type of cable has only the two twisted pairs TPA and TPB.
These can only be used for signal transfer. \cite{fwjaycar} \\

Because IEEE 1394 operates at high speeds, cables between devices should be no longer than 5 metres.
There should also be no more than 16 cable hops separating two nodes. \cite{fwjaycar}

\begin{figure}[H]
\centering
\includegraphics[width=.9\linewidth]{./images/fwTP.png}\caption{Primary type of IEEE 1394 cable \cite{fwjaycar}}
\label{fig:fwtp}
\end{figure}

\subsection{Arbitration}
\label{ssec:fwarb}
FireWire uses two kinds of arbitration depending on what kind of nodes are present on the bus.
Arbitration with only asynchronous devices and arbitration with isochronous devices.
If isochronous devices are present, arbitration is handled the same regardless of any asynchronous devices. \cite{fwlinux} \\

Asynchronous-only arbitration is based on fair scheduling with priorities.
After a 20$\mu$s gap, which is the bus idle time, a new arbitration round starts.
If a node wishes to transmit, it sends and arbitration request which propagates up to the root node.
If multiple nodes request arbitration at the same time, the node closest to the root node wins arbitration.
If nodes are the same distance from the root node, then the node connecting to the lowest root port wins arbitration.
Each node can speak at most once per arbitration round, after a 10$\mu$s gap a new rounds starts. \cite{fwlinux}\\

The isochronous arbitration is more complicated, because those devices already have guaranteed bandwidth.
Each arbitration round is approximately 125$\mu$s long.
This means that bandwidth is allocated as a portion of the 125$\mu$s interval.
The root node broadcasts a cycle start packet, which begins the arbitration round.
All interested nodes send an arbitration request as before and the winning node is still the node closest to the root.
After 0.04$\mu$s idle time the remaining nodes arbitrate for control of the bus.
Once each isochronous node has completed its transaction the time remaining in the 125$\mu$s interval is used for asynchronous transactions.
Up to 80\% of the bus bandwidth may be allocated to isochronous transactions, the remaining 20\% is left for asynchronous transactions. \cite{fwlinux}

\subsection{Conclusion}
When FireWire was first released it looked like it could become the standard of high speed interfaces.
USB 1.1 supported low-speed (1.5 Mbps) and full-speed (12 Mbps) devices.
This was very slow compared to FireWire's 400 Mbps. \\

Two things prevented FireWire's widespread adoption:
\begin{enumerate}
\item Apple got greedy, seeking a \$1 royalty per port for devices using its technology.
\item USB 2.0 arrived in 2000 with a 480 Mbps bandwidth and no additional royalty costs over those already being paid for USB 1.1 ports. Although USB 2.0 couldn’t match the real world throughput of FireWire 400, it didn’t really matter.
\end{enumerate}
