\section{KNX}
\label{sec:KNX}
KNX (abbreviation of Konnex), is an open standard for commercial and domestic
building automation.
An interesting aspect of KNX is that it can also communicate over power line.
Power line in function of G.hn was discussed in the previous chapter. In this
chapter KNX will be looked at and how it also takes advantage of existing wires
in homes.

Although take-up of power line transmission (PL110), it is a good
candidate for comparing it with the previous Power line communication (G.hn).
It should however be noted that they both aim to do different things. KNX is
focused on home automation. While G.hn is focused on networking.

\subsection{Background}
\label{ssec:knxBackground}
KNX evolved from three earlier standards; the European Home Systems Protocol
(EHS), BatiBUS, and the European Installation Bus (EIB or Instabus). It can use
twisted pair, powerline, RF, infrared or Ethernet links in a tree, line or star
topology.\cite{wikiKNX}

KNX is \textbf{not} based on a specific hardware platform. This means that it
can be controlled by an 8-bit MCU or a supercomputer.

It is administered by the KNX Association cvba, a non-profit organisation
governed by Belgian law which was formed in 1999. The KNX Association had 443
registered hardware and software vendor members from 44 nations as at 1 July
2018. It had partnership agreements with over 77,000 installer companies in 163
countries and more than 440 registered training centres. This is a
royalty-free open standard and thus access to the KNX specifications is
unrestricted.\cite{wikiKNX}\\


\subsection{Medium Attachment Unit (MAU)}
\label{ssec:knxMAU}
The Medium Attachment Unit is responsible for converting the frequency-coded
signals into values representing logical ones and zeros and vice versa.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/structure_of_the_mau.png}
\caption{Structure of the MAU (Example)\cite{KNXdocs}}
\label{fig:KNXStructOfMAU}
\end{figure}
\FloatBarrier

\subsubsection{Signal encoding}
\label{sssec:knxMAUSignalEncoding}
A signal of 105,6 kHz for a period of 833,3 $\mu$s corresponds to a logical
,,0'', a signal of 115.2 kHz for a period of 833,3 $\mu$s corresponds to
logical ,,1''. See figure \ref{fig:KNXSignalEncoding} for illustration.\cite{KNXdocs}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/knx_signal_encoding.png}
\caption{Signal encoding\cite{KNXdocs}}
\label{fig:KNXSignalEncoding}
\end{figure}
\FloatBarrier

These NRZ-signals are superimposed to the 230 V/50 Hz mains AC-voltage. The
maximum aptitude of the signal must be limited to 122 dB$\mu$V, measured with
CISPR 16-1 artificial mains network according EN 50065-1.
For lowest disturbance, the change between adjacent symbols shall be phase
continuous.\cite{KNXdocs}

\subsubsection{Overlapping of logical "0" or "1"}
\label{sssec:overlapping_of_logical_0_or_1_1}
Overlapping of logical "0" or "1"-symbols, e.g. the simultaneous transmission
of equal information at the same time of several MAUs, will result in fade-in /
fade-out effects. Due to slight frequency deviations between several MAUs the
signal will fade periodically with the difference of the MAU-frequencies. In
PL110 Powerline communication this case is avoided by setting a unique Group
Response Flag to each assigned Group Address.\cite{KNXdocs}

It is also possible for collision to occur. The probability of this sate is
minimized by special bus access mechanism.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/idealised_overlapping.png}
\caption{Idealized overlapping of 105,4 kHz and 115,2 kHz\cite{KNXdocs}}
\label{fig:idealised_overlapping}
\end{figure}
\FloatBarrier

\subsection{PL110 character overview}
\label{ssec:PL110_character_overview}
Each PL110 frame shall start with a training sequence and a preamble. These
shall not be coded. Each Data Link Layer octet shall be coded to a 12-bit
character (8 bits of data + 4 bits for error correction).

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/knx_character.png}
\caption{Character\cite{KNXdocs}}
\label{fig:knx_character}
\end{figure}
\FloatBarrier

During transmission and reception no time gaps are allowed between the bits of
a character.

\subsubsection{Frame structure}
\label{ssec:Frame_structure}
The datagram shall consist of training sequence, preamble I / II, LDPU+CS and
Domain Address.
Frame Check Sequence CS shall only be calculated with respect to LDPU.
It is noted that Twisted Pair 1 and Powerline 110 CS are identical.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/structure_acknowlege.png}
\caption{Structure of an Acknowledge Frame\cite{KNXdocs}}
\label{fig:Acknowledge Frame}
\end{figure}
\FloatBarrier


\subsection{Faulty transmission detection}
\label{ssec:faulty}
The error correction of the PL110 Physical Layer shall be done by Powerline
(12,8) block - coding.
Generation is calculated with the following matrix:

\begin{displaymath}
G =
    \begin{bmatrix}
    1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 \\
    0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 \\
    0 & 1 & 1 & 1 & 0 & 0 & 0 & 1 \\
    1 & 0 & 1 & 1 & 0 & 1 & 1 & 0 \\
    1 & 1 & 0 & 1 & 1 & 0 & 1 & 0
    \end{bmatrix}
    = \begin{bmatrix} E \\ T  \end{bmatrix}
\end{displaymath}

Coding shall result in an overhead of 4 bit referring to one octet. The hamming
- distance of this coding
shall be 3 (min). With this (12,8) - coding it shall be possible to correct
every single bit error in a 12 bit
character and to recognize some multiple errors.
The code shall be calculated by determining redundancy r as the function of the
transformation matrix T and the octet x:\cite{KNXdocs}

\begin{displaymath}
    r = T * x
\end{displaymath}

For decoding an estimation r` of the redundancy dependant on the incoming data
d must be performed.
The estimated redundancy shall be subtracted by the received redundancy du. The
result shall be a
syndrome with the value of s indicating the column of the error. Correction can
be done by inverting this
bit. For an error - free transmission the difference of r´ and du is 0. `

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/syndromes_erros.png}
\caption{Table of syndromes related to errors\cite{KNXdocs}}
\label{fig:error_syndromes}
\end{figure}
\FloatBarrier

For all calculations, GF2 arithmetic shall to be used:
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/galois-field.png}
\end{figure}
\FloatBarrier

Referring to above figure a syndrome value of 6 corresponds to an error in
column 3. Inverting bit number 4 leads to the corrected frame.\cite{KNXdocs}



\subsection{KNX Conclusion}
\label{ssec:KNX_Conclusion}
KNX has recently gained a lot of momentum, being adopted by more than 77,000
installer companies and the fact that it is a royalty-free open standard makes
for a great and robust standard.
Even though the power line standard has not really caught on it is still a
viable option when working with older installation where it is not feasible to
renovate the entire installation.

