\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}FireWire}{3}{section.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Terminology}{3}{subsection.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Address Space}{4}{subsection.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Transfer}{4}{subsection.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Line coding}{4}{subsection.2.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Twisted pairs}{5}{subsection.2.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6}Arbitration}{6}{subsection.2.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7}Conclusion}{7}{subsection.2.7}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}NuBus}{8}{section.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Background}{8}{subsection.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Specifications}{9}{subsection.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}NuBus transaction}{10}{subsection.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Conclusion}{11}{subsection.3.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}PC/104 bus}{12}{section.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Signal Line Definition}{12}{subsection.4.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.1}Address and data \cite {pc104Spec}}{12}{subsubsection.4.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2}Cycle Control \cite {pc104Spec}}{12}{subsubsection.4.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.3}Bus Control \cite {pc104Spec}}{13}{subsubsection.4.1.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.4}Interrupt \cite {pc104Spec}}{13}{subsubsection.4.1.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.5}DMA \cite {pc104Spec}}{13}{subsubsection.4.1.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Signal Timing}{14}{subsection.4.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1}8-bit memory and I/O timing}{14}{subsubsection.4.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2}16-bit memory and I/O timing}{15}{subsubsection.4.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.3}DMA read and write}{17}{subsubsection.4.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}PC/104 conclusion}{18}{subsection.4.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Power-line communication}{19}{section.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}G.hn}{19}{subsection.5.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Basics}{19}{subsection.5.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}Challenges}{20}{subsection.5.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4}Signal Modulation Techniques}{20}{subsection.5.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.4.1}Orthogonal frequency-division multiplexing (OFDM)}{20}{subsubsection.5.4.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.4.2}Quadrature amplitude modulation (QAM)}{21}{subsubsection.5.4.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.5}Error checking}{23}{subsection.5.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.5.1}Low-density parity-check code (LDPC)}{23}{subsubsection.5.5.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.6}Power-line communications conclusion}{26}{subsection.5.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}KNX}{27}{section.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Background}{27}{subsection.6.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Medium Attachment Unit (MAU)}{27}{subsection.6.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.1}Signal encoding}{28}{subsubsection.6.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.2}Overlapping of logical "0" or "1"}{28}{subsubsection.6.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}PL110 character overview}{28}{subsection.6.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.3.1}Frame structure}{29}{subsubsection.6.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.4}Faulty transmission detection}{29}{subsection.6.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.5}KNX Conclusion}{30}{subsection.6.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Final conclusion}{31}{section.7}% 
