#+TITLE: FireWire - NuBus - ISA/PC/104
#+AUTHOR: Sil Vaes
#+DATE:
#+LATEX_CLASS:  article
#+LATEX_CLASS_OPTIONS: [a4paper,10pt]
#+LATEX_HEADER: \usepackage[dutch]{babel}
#+LATEX_HEADER: \usepackage[margin=4cm]{geometry}
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage{libertine}
#+LATEX_HEADER: \usepackage{libertinust1math}
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \renewcommand*\familydefault{\sfdefault}
#+LATEX_HEADER: \setlength\parindent{0pt}

* FireWire
IEEE 1394.

* NuBus
NuBus is een 32-bit parallelle computerbus.
Aanvankelijk ontwikkeld bij MIT in samenwerking met Western Digital in 1979-1983.
Toen NuBus geïntroduceerd werdt, was dit een grote stap voorwaarts in vergelijking met de 8-bit bussen.
Wat ook revolutionair was, is dat NuBus 4 identificatie bits heeft zodat de kaart kan geïdentificeerd kan worden door host bij het opstarten.

Tegenwoordig wordt NuBus niet meer gebruikt. Want deze is obsoleet gemaakt door PCI en andere parallelle bussen.
Het is gestandariseerd in de IEEE 1196-1987, deze is nu ingetrokken.

Lijst van NuBus signalen:
| Utility signals             |  4 |
| Transaction control signals |  4 |
| Slot identification         |  4 |
| Address/data/parity         | 34 |
| Arbitration signals         |  5 |
| Power                       | 23 |
| Ground/reserved             | 22 |

#+CAPTION: NuBus transaction
#+NAME: fig:nubusTrans
[[./images/NubusTransaction.png]]

NuBus is a synchronous (10 MHz), multiplexed, multimaster bus that provides a fair arbitration mechanism.

(Bron: Developing for the Macintosh NuBus; B. G. Taylor; CERN, Geneva, Switzerland)

* PC/104 bus
Gebaseerd op ISA (Industry Standard Architecture).
