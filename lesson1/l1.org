#+LATEX: \setlength\parindent{0pt}
#+TITLE: Lesson 1
#+DATE: 20/09/2018

** Data rate limits
Aantal niveau's van een signaal verhogen verhoogt de kans op fouten omdat het marge tussen de niveau's verlaagt.

** Nyquist/Shannon
*** Nyquist:
Voor een noiseless kanaal:

$C = 2 * BW * \log_{2}(2^n)$

Met: C = capaciteit (kbps) en BW = bandbreedte (Hz).

*** Shannon:
Met noise:

$C = BW * log_{2} (1 + SNR)$

Met: SNR = signal to noise ratio.

$SNR_{Db} = 10 \log_{10} SNR \rightarrow SNR = SNR^{SNR_{Db}/10}$

Shannon geeft the hoogste limiet, Nyquist geeft het aantal niveau's die nodig zijn om dit limiet te bereiken.

** Performance
 - BW: capaciteit.
 - Throughput: aantal bits die erdoor kunnen.
 - Latency (delay): delay op een bit van begin tot einde.
 - Bandwidth-delay product = volume. ($BW * L$)

*** Bandwidth
Twee contexten:
 - BW in Hz, range van frequenties.
 - BW in bits per seconde, snelheid van transmissie (=capaciteit).

*** Propagation and transmission delay
 - Propagation speed: hoe snel een bit door het medium reist, van bron naar eindpunt. Kijkt enkel naar medium, niet naar de BW.
 - Transmission speed: de snelheid waarmee alle bits naar het eindpunt reizen. Kijkt naar BW. (Verschil tijd eerste en laatste bit)

Dit verschil kan bijvoorbeeld liggen aan overhead van het signaal (zeker weten dat iets correct aankomt).

 - Propagation delay: $PD = \frac{Distance}{Propagation\ speed}$
 - Transmission delay: $TD = \frac{Message\ size}{BW(bps)}$
 - Latency = PD + TD + Queueing time + Processing time

** Line coding
Als men 20 maal een nulsignaal ontvangt, hoe kan men weten of men data ontvangt of niet?
Om dit te vookomen is line coding nodig.

Bijvoorbeeld: -V = 0 en +V = 1, of -V -> +V = 1 en +V -> -V = 0.

 - Data rate: bits per second.
 - Signal rate: aantal signaalelementen per seconde, in bauds.

Doel: Data rate hoger en signal rate lager.
*** Signal rate
 - T_e: hoe snel men van niveau kan veranderen.
 - T_s is de slew rate.

Signal rate: $S = c * N * \frac{1}{r}$ [Bauds]

Met: N = data rate, c = case factor, r = ratio tussen data- en signal element.

Wanneer T_e kleiner is dan t_s, zal de output nooit de maximale waarde bereiken. $V_s= 2W_k$

De kortste afstand tussen twee niveau's is $\frac{1}{2W_k}$

Minimaal: $T_e = 2*t_s$.
