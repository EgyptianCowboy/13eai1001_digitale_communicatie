#+LATEX: \setlength\parindent{0pt}
#+TITLE: Lesson 3
#+DATE: 04/10/2018

* Block Coding
Om error detectie te doen moeten we redundancy toevoegen. Dit zijn extra bits bovenop de data bits.

Voor synchronisatie is er ook redundancy nodig. Transities zijn belangrijk.

Block coding gebeurt in 3 stappen; division, substitution en combination.

De resulterende bitstream voorkomen bepaalde bitcombinaties. Dit om bepaalde ongewenste eigenschappen te voorkomen (vb. DC-componenten, slechte synchronisatie).

Geschreven als xB/yB. $\rightarrow$ bitrate verhoogt met $\dfrac{y}{x}$.

Vervangt mB groepen naar nB data.

** Mapping
Er wordt een "tabel" bijgehouden om ontvangen data te vertalen. Dit is ook een soort van encryptie.

Als er combinaties over zijn, kunnen er control sequences gemaakt worden.

** Substitution
Dataframes worden door elkaar doorgestuurd.
Dit kan nodig zijn als er bijvoorbeeld te veel nullen of enen na elkaar gestuurd worden. Dit kan door de receiver verkeerd geïntepreteerd worden.

* Scrambling

De beste code is degene die de BW niet verhoogt en ongewenste eigenschappen voorkomt.

Wordt geïmplementeerd tegelijk met encoding, wordt on the fly gecreëerd.

Met een te lange string van enen of nullen worden deze vervangen met violation code.

Zie afbeeldingen in sheets.

* Analog-to-digital conversion (CH3)
Twee technieken:
 - Pulse code modulation
 - Delta modulation

** PCM (Pulse Code Modulation)
 - Sampling
 - Quantization
 - Binary encoding

Voor de sampling moet het analoge signaal gefilterd worden. Met het filteren moet het signaal niet vervormd worden.

*** Sampling
Drie sampling methodes (zie sheets voor afbeeldingen):
 - Ideal
 - Natural
 - Flattop
Ideal enkel als sampling freq = bitrate.

Verschil sampling lowpass/bandpass (200kHz lowpass $\rightarrow$ 400kHz sampling; 200kHz bandpass $\rightarrow$ onbekende sampling frequentie).

Met een te hoge sampling freq is er een kans op sampling ruis.

*** Quantization
Het resultaat van sampling zijn een serie van pulsen.

Signaal onderverdelen in niveau's, om deze dan op te delen over digitale codes.

$\Delta = \dfrac{(max-min)}{L}$
L = aantal niveau's.

Het middelste waarde van elke zone wordt de naam van elke zone (0 $\rightarrow$ L-1) en ook de waarde bij decoding. Hier zit dus altijd een fout op.

**** Quantization error
Wanneer het signaal gekwantiseerd wordt, wordt er een fout geïntroduceerd

Hoe meer niveau's/levels, hoe kleiner de error. Want de $\Delta$ wordt kleiner.

Maar hoe meer levels, hoe meer bits er nodig zijn om de samples te coderen. Dus verhoogt ook de bitrate.

***** SNQR
Signal to noise quantization ratio.

$ErrorRange: \dfrac{\Delta}{2}$



*** PCM Decoder
Gebruikt sample en hold ($\pm$ condensator).

** Delta modulation
Deze methode stuurt enkel het niveauverschil door.

Bijvoorbeeld: een één om één niveau te stijgen, een nul om één niveau te dalen.

Hier is de kans ook groot dat er veel enen en nullen na elkaar doorgestuurd worden (AMI gebruiken).

Deze methode is ook veel eenvoudiger/goedkoper dan een heel sample/quantization/decode systeem te gebruiken.

*** Componenten
Modulator:
 - Comparator
 - Delay unit
 - Staircase maker
Demodulator:
 - Staircase maker
 - Delay unit
 - Lowpass filter (DAC)

*** Delta PCM
Meer bits gebruiken om positieve/negatieve verschillen voor te stellen (quantization van het verschil).

Elke bit stelt de waarde van het verschil voor.

Hoe meer levels, hoe nauwkeuriger.

** Transmission modes
 - Serial
 - Parallel:
   - Synchroon
   - Asynchroon
   - Isochroon
*** Asynchroon
Start- en stopbit nodig om te communiceren.

De gap tussen start en stop is niet van belang (bij synchroon wel).

Systeem kan niet in een slaapstand zijn. Het systeem moet al wakker zijn om de startbit op tijd in te lezen.

*** Synchroon
Alle data zit in een frame. Er zit startdata (bytes bijvoorbeeld) aan het begin van het frame.

Gaps tussen frames zijn niet van belang. Gaps tussen bytes wel.

*** Isochroon
Hier zijn alle gaps van belang.

Er wordt constant data gestuurd. Dus het systeem moet constant wakker zijn.

Wordt gebruikt bij systemen met lage datarates.
