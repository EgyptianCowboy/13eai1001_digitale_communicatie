#+TITLE: Labo Digitale Communicatie
#+AUTHOR: Sil Vaes
#+DATE:
#+LATEX_CLASS:  article
#+LATEX_CLASS_OPTIONS: [a4paper,10pt]
#+LATEX_HEADER: \usepackage[dutch]{babel}
#+LATEX_HEADER: \usepackage[margin=4cm]{geometry}
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage{libertine}
#+LATEX_HEADER: \usepackage{libertinust1math}
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \renewcommand*\familydefault{\sfdefault}
#+LATEX_HEADER: \setlength\parindent{0pt}
#+LATEX_HEADER: \usepackage{amsmath}

* Lab 4: Image Rejection and DSB-SC
** Section 1: Image Rejection
  5. [@5] *Record the amplitude of the carrier frequency (the larger of the two peaks) in the power spectrum.*
Frequentie 915,1 MHz, -41 dB.

  6. [@6] *Set the transmitter to the image frequency.*

     b. Frequentie: 914,9 MHz, -41 dB.

     d. Er is geen verschil.

  7. [@7] *Record the amplitude of the carrier frequency on the power spectrum.*
Signaal is identiek, piek is ook -41 dB.

  8. [@8] *Do the two screenshots from steps 7 and 8 demonstrate the same change in shape, frequency, and amplitude as the previous comparison? Explain why or why not.*
De "negatieve" frequentie piek is eruit, deze image frequency wordt gefilterd.

** Section 2: DSB-SC
  6. [@6] *Observe the difference between the synchronized and unsynchronized waveforms of the demodulated output.*
Er is een heel klein verschil.

  7. [@7] *Was the AM receiver able to correctly demodulate the DSB-SC signal? Compare it with the screenshot from step 6 if needed.*
Het signaal is vervormd. Alles wat bij de receiver wordt geïnverteerd.

Signalen worden niet in fase samengesteld, er is geen phase synchonizer.

  8. [@8] *Verify that the synchronized demodulated output is correct, except possibly for being inverted.*
De phase synchronizer verbeterde de ongesynchoniseerde signalen.

  9. [@9] *Repeat for frequency offsets of 100 Hz and 1 kHz. Can your phase synchronizer handle the 1 kHz case?*
De phase synchonizer kan 100 Hz aan, bij 1kHz verandert de amplitude van het ontvangen signaal.

* Lab 5: Amplitude-Shift Keying
 3. [@3] *Run the transmitter and then stop it after a few seconds.*

    b. Null-to-Null BW = BW of the main lobe (most power)
20 kHw.

 4. [@4] *Rectangular pulses are rarely used in practice because of the very gradual spectral rolloff they produce. To improve spectral rolloff (which smooths out the output curve) we will apply a root-raised-cosine filter.*

    a. Rectangular filter: slower rolloff.

    b. Rolloff beter (higher gradient).

    c. Null-to-Null BW = 14800Hz (Smaller).

 6. [@6] *Run the transmitter and then run the receiver. Once the receiver has acquired a block of data, you may stop the transmitter. The receiver will stop on its own.*

    a. Minder harmonische, overshoot anders.

    b. Ja

 7. [@7] *Change the pulse shaping filter control at both the transmitter and the receiver to “root-raised-cosine”. The root-raised-cosine pulse is designed to minimize, or ideally eliminate, intersymbol interference.*
Meer harmonische betekent een "betere" filter.

 8. [@8] *To verify the transmitter and receiver are working together correctly as a system we will compare the received bits with the transmitted bits and verify that the bit patterns are the same.*

    c. Bits zijn correct doorgestuurd, maar de transmitter slewrate is anders. De receiver corrigeert dit.

** Questions
 1. [@1] *Relate the symbol rate to the null-to-null bandwidth of the transmitted signal for rectangular and root-raised-cosine pulses.*
In dit geval gaat door de soort pulsen de symbolrate/BW omhoog (rectangle vs root-raised-cosine).
 2. [@2] *Using the transmitted signal, compare the rates of spectral rolloff for rectangular and root-raised-cosine pulses. Did the addition of a filter improve spectral rolloff?*
Door gebruik te maken van RRC pulsen gaat de rolloff verbeteren (stijler).
 3. [@3] *Note that the IQ sampling rate at the receiver is different from the IQ sampling rate at the transmitter. Using a higher IQ sampling rate requires faster digital processing. What is the advantage to using a higher IQ sampling rate at the receiver?*
Minder noise/interference. Eerst filteren met offset frequency dan echte frequency.
 4. [@4] *Compare the rectangular and root-raised-cosine eye diagrams. Which has less intersymbol interference?*
Rectangular heeft meer intersymbol interference.
 5. [@5] *The transmitter is programmed to generate the same “frame” of 1000 symbols over and over. The receiver grabs a single block of 2000 symbols each time it is run. Can you identify, by examining the receiver’s baseband output plot, where the symbol sequence ends and starts over?*
Start sequency, even signaal in het midden (+- 0.5V)
 6. [@6] *In step 8, do the received bits match the transmitted bits? Based on this information, are the transmitter and receiver working together correctly?*
The bits matchen, ze werken dus correct.
* Lab 6: Frequency modulation and Frequency-Shift Keying
** Section 1: Frequency Modulation
 5. [@5] *The bandwidth of an FM signal is notoriously difficult to calculate analytically.*
De BW is niet moeilijk te berekenen, maar de BW utilization.

(4D, tijd niet megenomen) Bv: 80% op 0 - 500 Hz en 20% op 500 Hz+. Er is een slechte  BW utilization, maar dit is niet te zien op een spectrum.

 6. [@6] *For a more realistic set of FM spectra, set the message for three tones.*

    c. BW blijft hetzelfde, 3 tone 30 kHz peak deviation loopt iets sneller uit. Bij de 30 kHz 3 tone  liggen de lobes veel lager dan de 1 kHz.
 7. [@7] *One of the more curious, but also very useful, phenomena associated with FM is the so-called " capture effect."*

    e. Klopt.
** Section 2: Frequency-Shift  Keying
 3. [@3] *Run the transmitter. Use the large STOP button on the front panel to stop transmission connectors.*

    c. Null-to-Null BW = 30 kHz.

 4. [@4] *The rate of spectral rolloff of an FSK signal is determined primarily by the smoothness of the transmitted signal. Continuous-phase FSK has no discontinuities when the frequency of the transmitted signal changes, but there can be “corners” where the slope of the transmitted signal changes abruptly. To smooth out these corners, the message can be filtered before it is passed to the FM modulator.*

    c. BW = 40 kHz.

    e. Rolloff veel beter (hoger).

 5. [@5] *Change the pulse-shaping filter back to "none."*
Null-to-Null BW:
 - 20 kHz: 60 kHz.
 - 5 kHz: 15 kHz.
 7. [@7] *Run the transmitter, and then run the receiver.*

    e. Ze matchen, maar ze hebben een iets andere vorm.

    f. Sampling time = 0.0001s.

 8. [@8] *Set the transmitter’s peak frequency deviation to 20,000 and then to 2500 Hz.*

    b. 20 kHz = 0.3 en 2.5 kHz = 0.04.

    c. Amplitude ligt veel lager maar de data is hetzelfde. Dit komt omdat de overlap van channels zorgt voor een verzwakking van het signaal.

 9. [@9] *Return the peak frequency deviation to 5000 Hz.*
Met een Gaussian filter kan er veel meer data worden verstuurd.

    d. Er is veel minder overshoot op de baseband output. Er zijn wel meer golfvormen op het signaal (komt door de Gaussian filter = "lelijker").

* Lab 7: Binary Phase-Shift Keying
 3. [@3] *Run the transmitter. After a few seconds, use the large STOP button on the front panel to stop transmission.*

    b. GW = 9 kHz. Dit is verwacht.

 4. [@4] *Change the pulse shaping filter control to “none” to create rectangular pulses and run the transmitter again.*
Veel vlakker, de spectral rolloff is veel lager (slechter).
 7. [@7] *Run the transmitter. Keep the transmitter running and run the receiver 3 times (it will stop on its own once it acquires a block of data). This gives the filter settings a chance to adjust to the signal. If you’ve run the receiver long enough, the eye diagram should look like the one in Figure 7-1. If your eye diagram looks like the one in Figure 7-2, keep running the receiver. Once the receiver has acquired the third block of data, you may stop the transmitter.*

    b. De optimum sampling time is 97 µs.

    Er is veel distortion, dus hoge SNR.

    Interference is ongeveer 0.0282 dB.

 8. [@8] *Run the transmitter and then run the receiver several times, recording the BER each time.*

BER = $\frac{\#Errors}{TotalBits}$

BER = 1.0 = alle bits fout.

BER = 0.0 = geen bits fout.

 9. [@9] *Open the DPSK transmitter and receiver files provided (Lab7_Tx_DPSK and Lab7_Rx_DPSK). These files have the differential encoding steps included in their programming.*

    b. BER blijft 0.0. Er is veel minder interference.

** Questions
 1. [@1] *If the DPSK transmission input signal was: 0110010 what would the transmission output be? What would the receiver output be?*
0110010

 2. [@2] *Explain what would happen if you omitted the phase synchronization step in the receiver.*
De phase shift wordt niet op het juiste moment uitgelezen, dus verliest de receiver data.

 3. [@3] *Although BPSK is a suppressed-carrier version of ASK, we do not use an envelope detector to demodulate BPSK the way we did for ASK. Why? What would the receiver output be if we used an envelope detector for demodulation?*
De carrier wordt niet gevarieerd in amplitude, maar fase verschuiving. Dus is de envelope niet relevant, wnt enkel de fase verschuiving wordt uitgelezen.

Als de output van de envelope detector zou gebruikt worden zal de output constant zijn.


 4. [@4] *Bit error rate is a measure of performance of a digital communication system. What would the bit error rate be if the transmitter failed and the receiver received only noise? Explain your reasoning.*
Alle bits zouden fout zijn dus BER = 1.0. Of de BER zal constant variëren.

 5. [@5] *In the BPSK system, the eye diagram has the same amplitude every time you run the receiver. In the DPSK system, the amplitude of the eye diagram changes on every run. Explain why this happens. If noise were present, would the BER also change every time the DPSK receiver is run?*
Neen, de encoding haalt de fouten eruit.
* Lab 8: The Eye Diagram
 3. [@3] *Run the transmitter. Use the large STOP button on the front panel to stop transmission connectors.*

    b. Main lobe BW = 10.2 kHz. Baseband BW = 5.1 kHz.

 5. [@5] *Run the transmitter, and then run the receiver.*
\begin{align*}
V_w &= 0.05 \\
V_b &= 0.07 \\
EyeOpening &= \frac{2V_w}{V_b+V_w} \\
\Rightarrow EyeOpening &= 83.33\%
\end{align*}

 6. [@6] *Set the propagation delay $\tau$ to 50µs, set Use Channel to "on," and repeat step 5.*

| Propagation delay  |  V_w |   V_b | Eye Opening |
|--------------------+------+-------+------------|
| $\tau = 50 \mu s$  | 0.05 | 0.065 |     86.96% |
| $\tau = 100 \mu s$ | 0.05 | 0.064 |     87.71% |
| $\tau = 150 \mu s$ | 0.05 | 0.063 |     88.50% |

    c. Klopt, als de propagation delay omhoog gaat zal de intersymbol interference naar omlaag gaan

** Questions
 1. [@1] *Why are we not concerned if the BER turns out to be 1.0?*
De BER is irrelevant voor dit lab, het eye diagram is op de zelfde manier te analyseren.
 2. [@2] *Multipath propagation is common in the cellular telephone environment. Propagation delays can vary depending on the locations of the base station, the mobile unit, and nearby buildings, but these delays do not depend on symbol rate. However, with each new generation of cellular service the symbol rate increases. Based on your finding, discuss the relationship between symbol rate, eye opening, and BER.*
Als de symbol rate stijgt, zal de eye opening dalen en de BER omhoog gaan. Dit omdat er dan meer kans is op ISI.

 3. [@3] *Comment on the relationship between the eye opening and the respective propagation delay using data acquired from step 6.*
Delay tussen transmissies zorg ervoor dat er minder kans is dat de symbolen teglijkertijd zullen ontvangen worden.
